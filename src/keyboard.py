################################################################
# Uniformed keyboard inputs
################################################################

import pygame
from constants import DirectionKeys


class KeyboardInputs(object):
    """ Contains and returns a representation of the last direction
    key pressed."""
    
    def __init__(self):
        """ Constructor without arguments. """
        self._left = False
        self._right = False
        self._up = False
        self._down = False
        self.KEY_RESOURCES = {
            pygame.K_LEFT: [self._left, DirectionKeys.DIR_LEFT],
            pygame.K_RIGHT: [self._right, DirectionKeys.DIR_RIGHT],
            pygame.K_UP: [self._up, DirectionKeys.DIR_UP],
            pygame.K_DOWN: [self._down, DirectionKeys.DIR_DOWN]
        }
   
    def get_last_dir_key_pressed(self):
        """ Return the last direction key pressed, if any. """
        for key in self.KEY_RESOURCES:
            if self.KEY_RESOURCES[key][0]:
                return self.KEY_RESOURCES[key][1]
        return DirectionKeys.DIR_NONE
        
    def decode_keydown(self, key):
        """ Decode a keydown event, setting the eventual direction key to true. """
        if key in self.KEY_RESOURCES:
            self.KEY_RESOURCES[key][0] = True
            
    def decode_keyup(self, key):
        """ Decode a keydown event, setting the eventual direction key to false. """
        if key in self.KEY_RESOURCES:
            self.KEY_RESOURCES[key][0] = False
