################################################################
#  Global imports
################################################################
import json
import sprites
from constants import DirectionKeys, ScreenDefinitions, GhostKeys, PacmanKeys

################################################################
#  Game level
#  Contains the running data, needed to describe a level in each instant.
################################################################
class LIKeys(object):
    """
        Contains the dictionary keys for the info needed to initialize a level.
        
    """
    # Unused color definitions
    KEY_BACKGROUND_COLOR = 'bgcolor'            #
    KEY_EDGE_LIGHT_COLOR = 'edgelightcolor'     #
    KEY_EDGE_SHADOW_COLOR= 'edgeshadowcolor'    #
    KEY_FILL_COLOR = 'fillcolor'                #
    KEY_PELLET_COLOR = 'pelletcolor'            #
    
    KEY_FRUIT_TYPE = 'fruittype'                # Fruit type contained in the level
    KEY_LEVEL_DATA = 'leveldata'                # Definition of the walls and pelles
    KEY_LEVEL_HEIGHT = 'lvlheight'              # Heigh of the level
    KEY_LEVEL_WIDTH = 'lvlwidth'                # Width of the level
    
    # The following positions are all expressed in boad units.
    KEY_PACMAN_POSITION = 'pacmanposition'      # Pacman start position in the game
    KEY_FRUIT_POSITION = 'fruitposition'        # Fixed position of the fruits
    
    ALL = {KEY_BACKGROUND_COLOR, KEY_EDGE_LIGHT_COLOR, KEY_EDGE_SHADOW_COLOR, \
            KEY_FILL_COLOR, KEY_FRUIT_TYPE, KEY_LEVEL_DATA, KEY_LEVEL_HEIGHT,  \
            KEY_LEVEL_WIDTH, KEY_PELLET_COLOR, KEY_PACMAN_POSITION, KEY_FRUIT_POSITION}


WALL_RESOURCES = {
    1:   'DOOR_H',
    100: 'WALL_STRAIGHT_HORIZ',
    101: 'WALL_STRAIGHT_VERT',
    105: 'WALL_CORNER_LL',
    106: 'WALL_CORNER_LR',
    107: 'WALL_CORNER_UL',
    108: 'WALL_CORNER_UR',
    110: 'WALL_END_B',
    111: 'WALL_END_L',
    112: 'WALL_END_R',
    113: 'WALL_END_T',
    120: 'WALL_NUB',
    130: 'WALL_T_BOTTOM',
    131: 'WALL_T_LEFT',
    132: 'WALL_T_RIGHT',
    133: 'WALL_T_TOP',
    140: 'WALL_X'
}


BLANK_RESOURCES = {
    0:  'BLANK'
}


class PelletKeys(object):
    PELLET = 2
    POWERUP = 3


PELLET_RESOURCES_AND_VALUES = {
    PelletKeys.PELLET: ('PELLET', 10),
    PelletKeys.POWERUP: ('PELLET_POWER', 50)
}

FRUIT_RESOURCES_AND_VALUES = {
    0: ('FRUIT_0', 100),
    1: ('FRUIT_1', 300),
    2: ('FRUIT_2', 500),
    3: ('FRUIT_3', 700),
    4: ('FRUIT_4', 1000)
}


GHOST_RESOURCES = {
    GhostKeys.BLINKY: 'BLINKY',
    GhostKeys.PINKY: 'PINKY',
    GhostKeys.INKY: 'INKY',
    GhostKeys.CLYDE: 'CLYDE'
}

GHOSTS_FOOD_VALUE = {
    4:  200,
    3:  400,
    2:  800,
    1:  1600
}


class LevelInit(object):
    """
    Contains all the information needed to initialize the level.
    """
    def __init__(self):
        self.data = None
        
    def load_from_file(self,  file_name):
        with open(file_name) as data_file:
            self.data = json.load(data_file)

    def is_valid(self):
        '''Checks whether all necessary keywords are present in the json-object,
           whether height and width are not 0 and whether the datacontaining matrix
           is valid. '''
        return all([key in self.data.keys() for key in LIKeys.ALL]) and \
                self.data[LIKeys.KEY_LEVEL_WIDTH] > 0 and \
                self.data[LIKeys.KEY_LEVEL_WIDTH] <= ScreenDefinitions.GAME_HORIZ_TILES and \
                self.data[LIKeys.KEY_LEVEL_HEIGHT] > 0 and \
                self.data[LIKeys.KEY_LEVEL_HEIGHT] <= ScreenDefinitions.GAME_VERT_TILES


class FruitLife(object):
    """
    Contains the start and end life time of the fruits appearing during the level.
    """
    def __init__(self, start, end):
        self.start = start
        self.end = end
    
    def get_description(self):
        return (self.start, self.end)
        

class LevStatKeys(object):
    """
        Contains the dictionary keys for the statistics of the level
    """
    POINTS_GAINED = 0
    PELLETS_LEFT = 1
    POWERUPS_LEFT = 2
    FRUITS_LEFT = 3
    GHOSTS_LEFT = 4
    ALL = [POINTS_GAINED,  PELLETS_LEFT,  POWERUPS_LEFT,  FRUITS_LEFT, GHOSTS_LEFT]


class BoardSquare(object):
    """
        Contains the data of a single square of the game board.
    """
    NON_IDENTIFIED = -1
    SPACE = 0
    WALL = 1
    
    def __init__(self, type = NON_IDENTIFIED, fixed_sprite = None):
        """
        Constructor with arguments:
            type -- acceptable values are space and wall. 
                (Redundant, used for collision detection.)
            fixed_sprite -- sprite contained in the square.
        """
        self.type = type
        self.fixed_sprite = fixed_sprite
        self.extra_sprites = None


class Rect(object):
    """
    Contains a simple representation of a 2D rectangle.
    """
    def __init__(self, x1, x2, y1, y2):
        self.min_x = min(x1, x2)
        self.max_x = max(x1, x2)
        self.min_y = min(y1, y2)
        self.max_y = max(y1, y2)
        self.points = [(self.min_x, self.min_y), (self.min_x, self.max_y), \
                    (self.max_x, self.max_y), (self.max_x, self.min_y)]


def _precise_collision(rect1, rect2):
    """ Return True if there is an overlap between two rectangles."""
    for point in rect1.points:
        if _point_in_rectangle(point, rect2):
            return True
    for point in rect2.points:
        if _point_in_rectangle(point, rect1):
            return True
    return False
    

def _point_in_rectangle(point, rect):
    """ Return True if the point is included in the area of the rectangle. """
    assert(len(point) == 2)
    point_x = point[0]
    point_y = point[1]
    if rect.min_x <= point_x and point_x <= rect.max_x and \
        rect.min_y <= point_y and point_y <= rect.max_y:
        return True
    return False


class Transformation(object):
    """
        Transforms between the board and screen coordinates.
    """
    _SIZE = 2
    def __init__(self, multipliers, board_max):
        """
        Constructor with arguments:
            multipliers -- size in pixel along X, Y corresponding to a unit in the board.
        """
        self._multipliers = multipliers
        self._board_max = board_max
        assert(len(multipliers) == self._SIZE and len(board_max) == self._SIZE)

    def board_to_pixel(self, board_position):
        """ Receive a position on the board, and return the position of the 
        upper left corner of the corresponding rectangle in pixel. """
        assert(len(board_position) == self._SIZE)
        pixel_position = [board_position[i] * self._multipliers[i] for i in range(self._SIZE)]
        return pixel_position

    def pixel_to_board(self, pixel_position):
        """ Receive a position in pixel, and return the board square in which
        the position is contained."""
        assert(len(pixel_position) == self._SIZE)
        position = [int(pixel_position[i] / self._multipliers[i]) for i in range(self._SIZE)]
        return position
        
    def pixel_to_board_all(self, pixel_position, tolerance = 0):
        """ Receive the left upper corner of a square, and return the 
        squares touched by it."""
        assert(len(pixel_position) == self._SIZE)
        decrease = tolerance + 1
        # Gets the four corners of the square.
        all_pixel_positions = [
            pixel_position, 
            [pixel_position[0] + self._multipliers[0] - decrease, pixel_position[1]], 
            [pixel_position[0], pixel_position[1] + self._multipliers[1] - decrease], 
            [pixel_position[0] + self._multipliers[0] - decrease, pixel_position[1] + self._multipliers[1] - decrease], 
            ]
        
        # Adds the four positions, if acceptable.
        board_positions = []
        for iter_pixel_pos in all_pixel_positions:
            new_board_pos = self.pixel_to_board(iter_pixel_pos)
            if self._is_acceptable(new_board_pos):
                board_positions.append(new_board_pos)
        return board_positions
    
    def _is_acceptable(self, position):
        return 0 <= position[0] and position[0] <= self._board_max[0] and \
            0 <= position[1] and position[1] <= self._board_max[1]

    def pixel_to_rectangle(self, pixel_position):
        rect = Rect(pixel_position[0], pixel_position[0] + self._multipliers[0], \
                    pixel_position[1], pixel_position[1] + self._multipliers[1])
        return rect


class Level(object):
    """
        Contains, initializes and maintains the game board, movable sprites and 
        statistics defining a level.
    """
    
    def __init__(self, level_init, fruit_lives, powerup_time):
        """
            Constructor, with arguments:
                level_init -- LevelInit object.
                fruit_lives -- Start and end life times of the fruits.
                powerup_time -- Length of the power up, when ghosts are safe to eat.
        """
        self._level_init = level_init
        self._fruit_lives = fruit_lives
        self._powerup_time = powerup_time
        self._ghost_death_time = powerup_time
        
        # Statistics initialization.
        self._statistics = {
            LevStatKeys.POINTS_GAINED: 0,  
            LevStatKeys.PELLETS_LEFT: 0,  
            LevStatKeys.POWERUPS_LEFT: 0, 
            LevStatKeys.FRUITS_LEFT: 0,
            LevStatKeys.GHOSTS_LEFT: 4
            }
        # Transformation initialization
        multipliers = ScreenDefinitions.TRANSFORMATION
        board_max = (self._level_init.data[LIKeys.KEY_LEVEL_WIDTH] - 1, self._level_init.data[LIKeys.KEY_LEVEL_HEIGHT] - 1)
        self._transformation = Transformation(multipliers, board_max)
        # Board data structures.
        self._max_position = ScreenDefinitions.GAME_SIZE
        self._board = [[]]
        self._movable_sprites = {}
        self._init_board_and_ghosts()
        self._init_fruits()
        self._init_pacman()
        
    
    # Initialization functions
    def _init_board_and_ghosts(self):
        """
        Change self._board, self._movable_sprites
        
        Generate a representation of the game board, containing 
        the sprites of walls, pellets and power pellets. It is 
        divided into squares each square containing a BoardSquare object.
        
        Generate 4 sprites, each representing a ghost and add them to the 
        self._movable_sprites dictionary.        
        
        """        
        rows = self._level_init.data[LIKeys.KEY_LEVEL_HEIGHT]
        cols = self._level_init.data[LIKeys.KEY_LEVEL_WIDTH]
        board_init = self._level_init.data[LIKeys.KEY_LEVEL_DATA]
        
        self._max_position = (cols * ScreenDefinitions._TILE_SIZE - 1, rows*ScreenDefinitions._TILE_SIZE - 1)
        self._board = [[BoardSquare() for x in range(cols)] for y in range(rows)]
        for x in range(cols):
            for y in range(rows):
                square_type = board_init[y][x]
                pixel_position = self._transformation.board_to_pixel([x, y])
                if square_type in WALL_RESOURCES:
                    # Walls or doors
                    self._board[y][x].type = BoardSquare.WALL
                    resource_id = WALL_RESOURCES[square_type]
                    frames = [resource_id]
                    self._board[y][x].fixed_sprite = sprites.Wall(pixel_position, frames)
                elif square_type in PELLET_RESOURCES_AND_VALUES:
                    # Pellets or power pellets
                    self._board[y][x].type = BoardSquare.SPACE
                    resource_id = PELLET_RESOURCES_AND_VALUES[square_type][0]
                    frames = [resource_id]
                    value = PELLET_RESOURCES_AND_VALUES[square_type][1]
                    self._board[y][x].fixed_sprite = sprites.Pellet(pixel_position, square_type, value, frames)
                    if square_type == 2:
                        self._statistics[LevStatKeys.PELLETS_LEFT] += 1
                    elif square_type == 3:
                        self._statistics[LevStatKeys.POWERUPS_LEFT] += 1
                elif square_type in GHOST_RESOURCES:
                    # Ghosts
                    ghost_id = square_type
                    resource_id = GHOST_RESOURCES[ghost_id]
                    all_frames = {}
                    all_frames[sprites.GhostStates.DANGEROUS] = [resource_id]
                    all_frames[sprites.GhostStates.SAFE] = ['SAFE_GHOST']
                    all_frames[sprites.GhostStates.DEAD] = ['DEAD_GHOST']
                    board_position = [x, y]
                    pixel_position = self._transformation.board_to_pixel(board_position)
                    self._movable_sprites[ghost_id] = sprites.Ghost(pixel_position, self._max_position, ghost_id, all_frames)
                else:
                    # Blank area
                    self._board[y][x].type = BoardSquare.SPACE
                    resource_id = BLANK_RESOURCES[0]
                    frames = [resource_id]
                    self._board[y][x].fixed_sprite = sprites.Blank(pixel_position, frames)
        
    def _init_fruits(self):
        """
        Change self._board
        The fruit sprites will be initially generated as invisible, and 
        assigned hardcoded positions inside the labyrinth.
        """
        # Common data to all fruits.
        board_position = self._level_init.data[LIKeys.KEY_FRUIT_POSITION]
        x = board_position[0]
        y = board_position[1]
        pixel_position = self._transformation.board_to_pixel(board_position)
        fruit_id = self._level_init.data[LIKeys.KEY_FRUIT_TYPE]
        if not fruit_id in FRUIT_RESOURCES_AND_VALUES:
            fruit_id = 0
        frames = [FRUIT_RESOURCES_AND_VALUES[fruit_id][0]]
        value = FRUIT_RESOURCES_AND_VALUES[fruit_id][1]
        # Inits the list of fruits.
        self._board[y][x].extra_sprites = [sprites.Fruit(pixel_position, fruit_id, value, frames, life.get_description()) for life in self._fruit_lives]
        # Refreshes the fruit related statistic
        self._statistics[LevStatKeys.FRUITS_LEFT] = len(self._board[y][x].extra_sprites)

    def _init_pacman(self):
        """
        Change self._movable_sprites
        Generate a sprite for the pacman and add it inside the 
        self._movable_sprites dictionary using the key 
        defined in PacmanKeys.
        """
        board_position = self._level_init.data[LIKeys.KEY_PACMAN_POSITION]
        pixel_position = self._transformation.board_to_pixel(board_position)
        pacman_id = PacmanKeys.PAC0
        all_frames = {
            DirectionKeys.DIR_NONE: ['PACMAN'],
            DirectionKeys.DIR_LEFT: ['PACMANL_' + str(i+1) for i in range(8)],
            DirectionKeys.DIR_RIGHT: ['PACMANR_' + str(i+1) for i in range(8)],
            DirectionKeys.DIR_UP: ['PACMANU_' + str(i+1) for i in range(8)],
            DirectionKeys.DIR_DOWN: ['PACMAND_' + str(i+1) for i in range(8)]
        }
        self._movable_sprites[pacman_id] = sprites.Pacman(pixel_position, self._max_position, pacman_id, all_frames)
        
    # Animation
    def animate(self):
        """ Animate all the sprites, and get the currently visible image from all."""
        rv = []
        for row in self._board:
            for square in row:
                if square.fixed_sprite and not square.extra_sprites:
                    rv.append(square.fixed_sprite.animate())
                elif square.extra_sprites:
                    for fruit in square.extra_sprites:
                        descriptor = fruit.animate()
                        if descriptor:
                            rv.append(descriptor)
        for id in self._movable_sprites:
            rv.append(self._movable_sprites[id].animate())
        return rv
        
    # Movement
    def set_pacman_direction(self, direction):
        self._movable_sprites[PacmanKeys.PAC0].set_direction(direction)
        
    def set_ghost_direction(self,  ghost_id,  direction):
        self._movable_sprites[ghost_id].set_direction(direction)
    
    def get_next_position_for_all(self):
        """ Get the next position for all the movable sprites. 
            Return a dictionary with the IDs of the sprites as keys, 
            and the next position as values."""
        rv = {}
        for key in self._movable_sprites:
            rv[key] = self._movable_sprites[key].get_next_position()
        return rv
    
    def move_all_to_position(self,  positions):
        """ Move some or all of the movable sprites to a new position. 
            positions -- dictionary with the IDs of the sprites as keys, 
                and the next position as values
            """
        for key in positions:
            if key in self._movable_sprites:
                self._movable_sprites[key].move_to_position(positions[key])

    def get_pacman_position(self):
        return self._movable_sprites[PacmanKeys.PAC0].get_position()
    
    def get_ghosts_position(self):
        rv = {}
        for key in GHOST_RESOURCES:
            rv[key] = self._movable_sprites[key].get_position()
        assert(len(rv) == 4)
        return rv
    
    def get_labyrinth(self):
        return self._board

    # Collision detection
    def cd_pacman_with_wall(self, pacman_pixel_position):
        """
        Check if in the next step pacman is going to collide with a wall.
            pacman_pixel_position -- pacman position in pixel.
        Return None if no collision, colliding wall otherwise.
        """
        board_positions = self._transformation.pixel_to_board_all(pacman_pixel_position, 2)
        for position in board_positions:
            x = position[0]
            y = position[1]
            try:
                if self._board[y][x].type == BoardSquare.WALL:
                    return self._board[y][x].fixed_sprite
            except:
                print 'exception in cd_pacman_with_wall()'
                for pos in board_positions:
                    print pos
        return None

    def cd_pacman_with_ghost(self, pacman_pixel_position):
        """
        Check if pacman is going to collide with any of the ghosts.
            pacman_position -- couple of integers, representing pacman position.
        Return None if no collision, colliding ghost otherwise.
        """
        for ghost_id in GHOST_RESOURCES:
            # Rectangle around pacman
            rect_pacman = self._transformation.pixel_to_rectangle(pacman_pixel_position)
            # Rectangle around the ghost
            ghost_position = self._movable_sprites[ghost_id].get_position()
            rect_ghost = self._transformation.pixel_to_rectangle(ghost_position)
            # Precise collision between the two rectangles
            if _precise_collision(rect_pacman, rect_ghost):
                return self._movable_sprites[ghost_id]
        return None
        
    def cd_pacman_with_pellet(self, pacman_pixel_position):
        """
        Check if pacman is going to collide with a pellet sprite.
            pacman_position -- couple of integers, representing pacman position.
        Return None if no collision, colliding pellet otherwise.
        """
        board_positions = self._transformation.pixel_to_board_all(pacman_pixel_position)
        for position in board_positions:
            x = position[0]
            y = position[1]
            if self._board[y][x].fixed_sprite and type(self._board[y][x].fixed_sprite) == sprites.Pellet:
                return self._board[y][x].fixed_sprite
        return None
  
    def cd_pacman_with_fruit(self, pacman_pixel_position):
        """
        Check if pacman is going to collide with any of the food sprites.
            pacman_position -- couple of integers, representing pacman position.
        Return None if no collision, colliding fruit otherwise.
        """
        board_positions = self._transformation.pixel_to_board_all(pacman_pixel_position)
        for position in board_positions:
            x = position[0]
            y = position[1]
            if self._board[y][x].extra_sprites:
                for fruit in self._board[y][x].extra_sprites:
                    if fruit.is_alive():
                        return fruit
        return None

    # Statistic and status change management.
    def get_statistic(self, id):
        """ Return the value of a single statistic of the level"""
        assert(id in self._statistics)
        return self._statistics[id]
   
    def eat_food(self, food):
        """ Receive a pellet or powerup to be eaten. Refresh the statistics of 
        the total points and of the remaining food elements, and removes the 
        eaten food from the board."""
        assert(food)
        # Refreshes the points with the value of food.
        value = food.get_value()
        self._statistics[LevStatKeys.POINTS_GAINED] += value
        # Refreshes the statistic of food left.
        if food.get_id() == PelletKeys.PELLET:
            self._statistics[LevStatKeys.PELLETS_LEFT] -= 1
        elif food.get_id() == PelletKeys.POWERUP:
            self._statistics[LevStatKeys.POWERUPS_LEFT] -= 1
            self._set_ghosts_safe()
        else:
            print food.get_id()
            assert(False)  # Wrong ID of the food
        # Removes the eaten food from the board, and transforms
        # the cleaned square into a blank one.
        board_position = self._transformation.pixel_to_board(food.get_position())
        x = board_position[0]
        y = board_position[1]
        assert(self._board[y][x].fixed_sprite)  # Why are you trying to eat food that was deleted?
        self._board[y][x].type = BoardSquare.SPACE
        resource_id = BLANK_RESOURCES[0]
        frames = [resource_id]
        pixel_position = self._transformation.board_to_pixel([x, y])
        self._board[y][x].fixed_sprite = sprites.Blank(pixel_position, frames)        
        
    def _set_ghosts_safe(self):
        """ Set all the ghosts in the safe state, and initializes their food
        value."""
        ghosts_left = 4
        self._statistics[LevStatKeys.GHOSTS_LEFT] = ghosts_left
        for id in GHOST_RESOURCES:
            self._movable_sprites[id].set_state(sprites.GhostStates.SAFE)
            self._movable_sprites[id].set_safe_state_length(self._powerup_time)
            self._movable_sprites[id].set_food_value(GHOSTS_FOOD_VALUE[ghosts_left])

    def eat_ghost(self, ghost_id):
        """ Eat a ghost, returning it into a dead state. Change the food value 
        of all the other ghosts."""
        self._movable_sprites[ghost_id].set_state(sprites.GhostStates.DEAD)
        self._movable_sprites[ghost_id].set_safe_state_length(self._ghost_death_time)
        ghosts_left = self._statistics[LevStatKeys.GHOSTS_LEFT]
        if ghosts_left > 1:
            ghosts_left -= 1
            self._statistics[LevStatKeys.GHOSTS_LEFT] = ghosts_left
            for id in GHOST_RESOURCES:
                self._movable_sprites[id].set_food_value(GHOSTS_FOOD_VALUE[ghosts_left])

    def is_powerup_in_effect(self):
        """ Return True if the powerup is still in effect. """
        return all([self._movable_sprites[id].is_safe() for id in GHOST_RESOURCES])
    
    def refresh_fruits(self):
        """ Delete dead fruits from the list, and refresh their statistic. """
        board_position = self._level_init.data[LIKeys.KEY_FRUIT_POSITION]
        x = board_position[0]
        y = board_position[1]
        for fruit in self._board[y][x].extra_sprites:
            if fruit.is_dead():
                self._board[y][x].extra_sprites.remove(fruit)
        self._statistics[LevStatKeys.FRUITS_LEFT] = len(self._board[y][x].extra_sprites)
    
    def eat_fruit(self, fruit):
        """ Receive a fruit to eat. Refresh the statistic of level points, and 
        fruits left, and remove the fruit from the board. """
        assert(fruit and fruit.is_alive())
        # Refresh the statistics.
        value = fruit.get_value()
        self._statistics[LevStatKeys.POINTS_GAINED] += value
        # Remove the fruit from the board.
        board_position = self._transformation.pixel_to_board(fruit.get_position())
        x = board_position[0]
        y = board_position[1]
        assert(self._board[y][x].extra_sprites and len(self._board[y][x].extra_sprites) > 0)
        for fruit in self._board[y][x].extra_sprites:
            if fruit.is_alive():
                self._board[y][x].extra_sprites.remove(fruit)
                
    def reinitialize_pacman(self):
        """ Called after a life is lost, resets Pacman in his initial position, 
        cleaning at the same time whatever direction of movement he might have
        had. """
        board_position = self._level_init.data[LIKeys.KEY_PACMAN_POSITION]
        pixel_position = self._transformation.board_to_pixel(board_position)
        pacman_id = PacmanKeys.PAC0
        self._movable_sprites[pacman_id].move_to_position(pixel_position)
        self._movable_sprites[pacman_id].set_direction(DirectionKeys.DIR_NONE)
        
    def get_fruit_id(self):
        fruit_id = self._level_init.data[LIKeys.KEY_FRUIT_TYPE]
        return FRUIT_RESOURCES_AND_VALUES[fruit_id][0]
