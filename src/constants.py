################################################################
#  Common definitions and classes shared by all modules.
################################################################


class DirectionKeys(object):
    """ Defines the keys for movement directions. """
    DIR_NONE = -1
    DIR_LEFT = 0
    DIR_RIGHT = 1
    DIR_UP = 2
    DIR_DOWN = 3
    ALL = [DIR_NONE, DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN]
    
    
class ScreenDefinitions(object):
    """ Defines positions and sizes of the game areas on screen. """
    FRAMES_PER_SECOND = 20
    
    _TILE_SIZE = 16
    GAME_HORIZ_TILES = 35
    GAME_VERT_TILES = 50

    TRANSFORMATION = (_TILE_SIZE, _TILE_SIZE)
    
    _WIDTH = _TILE_SIZE * GAME_HORIZ_TILES
    _HEIGHT = _TILE_SIZE * (GAME_VERT_TILES + 2)
    
    SCREEN_SIZE = (_WIDTH, _HEIGHT)
    GAME_SIZE = (_WIDTH, _TILE_SIZE * GAME_VERT_TILES)

    INCREASE_POSITION = (_TILE_SIZE, 0)

    BOARD_BASE_POSITION = (0, 0)
    
    TOTAL_POINTS_BASE_POSITION = (0, int(_HEIGHT - _TILE_SIZE))
    LEVEL_BASE_POSITION = (int(_WIDTH / 2), int(_HEIGHT - _TILE_SIZE))
    LIVES_BASE_POSITION = (0, int(_HEIGHT - 2*_TILE_SIZE))
    FRUITS_BASE_POSITION = (int(_WIDTH / 2), int(_HEIGHT - 2*_TILE_SIZE))
    
    @staticmethod
    def get_increased_position(index):
        position = [coord * index for coord in ScreenDefinitions.INCREASE_POSITION]
        return position
    


class GhostKeys(object):
    BLINKY = 10
    PINKY = 11
    INKY = 12
    CLYDE = 13
    ALL = {BLINKY, PINKY, INKY, CLYDE}
    
class PacmanKeys(object):
    PAC0 = 'PAC0'
