################################################################
#  Imports
################################################################
import os
import pygame

import resources
from enginegame import GameEngine
from enginegraphic import GraphicEngine
from enginesound import SoundEngine
import sprites
from constants import DirectionKeys, ScreenDefinitions
import keyboard


################################################################
#  Global variables
################################################################

# Resources
level_resources = None
image_resources = None
sound_resources = None

# Pygame 
clock = None

# Engines
graphic_engine = None
sound_engine = None
game_engine = None

# Controllers
keyboard_controller = None


################################################################
#  Initialization
################################################################
def game_init():
    """
        Initializes first the resources and then the engines.
    """
    init_level_resources()
    init_image_resources()
    init_sound_resources()
    init_pygame()
    init_engines()
    init_controllers()

def init_level_resources():
    """ Initializes the level resources.
    Takes the files from the folder 'levels' and checks if the file is valid
    if not valid: create a default one.
    """    
    global level_resources
    level_resources = {}

    # Folder containing the level resources
    folder_name = "./res/levels/"
    
    # Load all the resources contained in the level dictionary. 
    # If one can't be loaded, then loads a default level.
    for key in resources.LevelResourceDict:
        my_resource = resources.Resource("LEVEL", folder_name  + resources.LevelResourceDict[key])
        if my_resource.is_valid():
            level_resources[key] = my_resource
        else:
            level_resources[key] = default_level_resource()

def init_image_resources():
    """ Initializes the image resources 
    takes the images from the folders 'sprite', 'tiles' and 'backgrounds' and checks if the image is valid
    """    
    global image_resources
    image_resources = {}
    
    # Folder containing the different image resources: sprites, tiles, backgrounds and texts.
    folder_names = ["./res/sprite/",  "./res/tiles/",  "./res/backgrounds/", "./res/text/"]

    for key in resources.ImageResourceDict:
        for folder_name in folder_names:
            my_resource = resources.Resource("IMAGE", folder_name + resources.ImageResourceDict[key])
            if my_resource.is_valid():
                break
            else:
                my_resource = None
        if my_resource != None:
            image_resources[key] = my_resource
        else:
            image_resources[key] = default_image_resource()


def init_sound_resources():
    """ Initializes the sound resources 
    takes the sounds from the folder 'sounds' and checks if the sound is valid
    """
    global sound_resources
    sound_resources = {}
    
    # Folder containing the sound resources
    folder_name = "./res/sounds/"

    for key in resources.SoundResourceDict:
        my_resource = resources.Resource("SOUND", folder_name  + resources.SoundResourceDict[key])
        if my_resource.is_valid():
            sound_resources[key] = my_resource
        else:
            sound_resources[key] = default_sound_resource()

# Default resources
DEF_RESOURCES_FOLDER = './res/default/'     # Base folder of default resources
DEBUG_CHECK_RESOURCES = True                # Assert if default resources are even needed.

def default_level_resource():
    """ Initialize the default level resource. """
    if DEBUG_CHECK_RESOURCES:
        assert(False)
    rv = resources.Resource("LEVEL",  DEF_RESOURCES_FOLDER + resources.DefaultResourcesDict['DEFAULT_LEVEL'])
    assert(rv.is_valid())
    return rv

def default_image_resource():
    """ Initialize the default image resource. """
    if DEBUG_CHECK_RESOURCES:
        assert(False)
    rv = resources.Resource("IMAGE",  DEF_RESOURCES_FOLDER + resources.DefaultResourcesDict['DEFAULT_IMAGE'])
    assert(rv.is_valid())
    return rv

def default_sound_resource():
    """ Initialize the default sound resource. """
    if DEBUG_CHECK_RESOURCES:
        assert(False)
    rv = resources.Resource("SOUND",  DEF_RESOURCES_FOLDER + resources.DefaultResourcesDict['DEFAULT_SOUND'])
    assert(rv.is_valid())
    return rv


def init_pygame():
    global clock
    clock = pygame.time.Clock()
    pygame.init()


def init_engines():
    global graphic_engine
    graphic_engine = GraphicEngine(ScreenDefinitions.SCREEN_SIZE)
    graphic_engine.load_resources(image_resources)
    
    global sound_engine
    sound_engine = SoundEngine()
    sound_engine.load_resources(sound_resources)
    
    global game_engine
    game_engine = GameEngine()
    game_engine.load_resources(level_resources)
    game_engine.start()
    
    
def init_controllers():    
    global keyboard_controller
    keyboard_controller = keyboard.KeyboardInputs()
   

################################################################
#  Run
################################################################
def game_run():
    """
    Generates a single frame of the game, and shows and plays it.
    """
    # Game engine
    is_done = game_engine.generate_frame(keyboard_controller)
    
    # Graphic engine
    image_descriptors = game_engine.get_frame_images()
    graphic_engine.view_frame(image_descriptors)
    
    # Sound engine
    sound_keys = game_engine.get_frame_sounds()
    sound_engine.play_frame(sound_keys)
    
    return is_done


################################################################
#  Cleanup
################################################################
def game_cleanup():
    """
    Call the cleanup and resource releasing routines for all the 
    engines.
    """
    game_engine.cleanup()
    graphic_engine.cleanup()
    sound_engine.cleanup()


################################################################
#  Game main loop
################################################################
if __name__ == '__main__':
    """
        Main function of the game: initalizes the game, runs the
        main event loop, and cleans up at the end.
    """
    
    # Initialization
    game_init()

    # Game run
    done = False
    while not done:
        done = game_run()
        clock.tick(ScreenDefinitions.FRAMES_PER_SECOND)
        # Events check
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    done = True
                else:
                    keyboard_controller.decode_keydown(event.key)
            elif event.type == pygame.KEYUP:
                keyboard_controller.decode_keyup(event.key)
                    
    # Closing messages and cleanup
    game_cleanup()
