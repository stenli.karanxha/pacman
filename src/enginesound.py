#! /usr/bin/env python
################################################################
#  Sound engine
################################################################

class SoundEngine(object):
    """
    Class SoundEngine allows to play the sounds of pacman
    """
    
    def __init__(self):
        """ constructor(self) -> SoundEngine
        
        Constructor of the SoundEngine imports needed resources
        """
        from pygame import mixer
        self.NAME_OF_BACKGROUND_TRACK = 'PARADOX'
        self.mx = mixer
        self.mx.pre_init(22050,16,2,512)
        self.mx.init()
        self.pelletnr = 0
        self.sounds = {}

    def start_background_music(self):
        """ start_background_music(self) -> None

        starts background soundtrack self.NAME_OF_BACKGROUND_TRACK
        """
        self.mx.music.play(1000,0.0)


    def stop_background_muxic(self):
        """ start_background_music(self) -> None

        stops background soundtrack self.NAME_OF_BACKGROUND_TRACK
        """
        self.mx.music.fadeout(300)
        
    def load_resources(self,  sound_resources):
        """ load_resources(self, Dict(Resource)) -> None

        loads all resources as sounds into Dict: self.souncs
        """
        from resources import Resource
        self.sounds = dict(zip(sound_resources.keys(),[self.mx.Sound(_.file_name) for _ in sound_resources.values()]))
        
        # What this ugly line does (in a more performant way):
        #for resource in sound_resources.keys():
        #   self.sounds[resource] = self.mx.Sound(sound_resources[resource].file_name)
        
        if self.NAME_OF_BACKGROUND_TRACK in sound_resources.keys():
            self.mx.music.load(sound_resources[self.NAME_OF_BACKGROUND_TRACK].file_name)
            self.start_background_music()

    def play_sound(self, sound_key):
        """ play_sound(self, String) -> None

        plays a sound with the given key
        """
        if sound_key == 'PELLET':
            if self.pelletnr == 0:
                sound_key = 'PELLET_1'
            else:
                sound_key = 'PELLET_2'
            self.pelletnr = (self.pelletnr+1)%2
        if sound_key in self.sounds.keys():
            self.sounds[sound_key].play()
            
    def get_frame_sounds(self):
        """ play_frame(self) -> List(String)

        returns the possible sounds to play
        
        ???
        """
        return list(self.sounds.keys())

    def play_frame(self,  sound_keys):
        """ play_frame(self, List(String)) -> None

        plays all sounds in the list
        
        ???
        """
        for sound_key in sound_keys:
            self.play_sound(sound_key)

    def cleanup(self):
        """ cleanup(self) -> None

        calls the deconstructor
        
        ???
        """
        self.__del__

    def __del__(self):
        """ deconstructor(self) -> None       
        """
        self.mx.quit()
        self.sounds = None  
        self.pelletnr = None
        self.mx = None


# Some simple outputs since audio output is hard to test in Unittests
if __name__ == '__main__':
        import resources
        test_pelletnr = 0
        sound_resources = {}  
        folder_name = "./res/sounds/"
        for key in resources.SoundResourceKeyDict:
            my_resource = resources.Resource("SOUND", folder_name  + resources.SoundResourceKeyDict[key])
            if my_resource.is_valid():
                sound_resources[key] = my_resource
            else:
                sound_resources[key] = default_sound_resource()
        se = SoundEngine()
        se.load_resources(sound_resources)
        import time
        time.sleep(3)
        for x in range(0, 20):
            se.play_sound('PELLET')
        time.sleep(20)
        
