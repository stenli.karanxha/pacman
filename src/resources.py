################################################################
#  Definition of the keys and types for all the used resources.
################################################################

ResourceTypeDict = {
    'LEVEL' : 0,
    'IMAGE' : 1,
    'SOUND' : 2
}

LevelResourceDict = {
    'LEVEL_0' : 'lv0.json',
    'LEVEL_1' : 'lv1.json',
    'LEVEL_2' : 'lv2.json',
    'LEVEL_3' : 'lv3.json',
    'LEVEL_4' : 'lv4.json',
    'LEVEL_5' : 'lv5.json',
    'LEVEL_6' : 'lv6.json',
    'LEVEL_7' : 'lv7.json',
    'LEVEL_8' : 'lv8.json',
    'LEVEL_9' : 'lv9.json',
    'LEVEL_10' : 'lv10.json',
    'LEVEL_11' : 'lv11.json',
    'LEVEL_12' : 'lv12.json'
    }

ImageResourceDict = {
    # Sprites
    'FRUIT_0' : 'fruit 0.gif',
    'FRUIT_1' : 'fruit 1.gif',
    'FRUIT_2' : 'fruit 2.gif',
    'FRUIT_3' : 'fruit 3.gif',
    'FRUIT_4' : 'fruit 4.gif',
    'BLINKY' : 'ghost-blinky.gif',
    'PINKY' : 'ghost-pinky.gif',
    'INKY' : 'ghost-inky.gif',
    'CLYDE' : 'ghost-clyde.gif',
    'SAFE_GHOST' : 'ghost-safe.gif',
    'DEAD_GHOST' : 'ghost-glasses.gif',
    'PACMAND_1' : 'pacman-d 1.gif',
    'PACMAND_2' : 'pacman-d 2.gif',
    'PACMAND_3' : 'pacman-d 3.gif',
    'PACMAND_4' : 'pacman-d 4.gif',
    'PACMAND_5' : 'pacman-d 5.gif',
    'PACMAND_6' : 'pacman-d 6.gif',
    'PACMAND_7' : 'pacman-d 7.gif',
    'PACMAND_8' : 'pacman-d 8.gif',
    'PACMANL_1' : 'pacman-l 1.gif',
    'PACMANL_2' : 'pacman-l 2.gif',
    'PACMANL_3' : 'pacman-l 3.gif',
    'PACMANL_4' : 'pacman-l 4.gif',
    'PACMANL_5' : 'pacman-l 5.gif',
    'PACMANL_6' : 'pacman-l 6.gif',
    'PACMANL_7' : 'pacman-l 7.gif',
    'PACMANL_8' : 'pacman-l 8.gif',
    'PACMANR_1' : 'pacman-r 1.gif',
    'PACMANR_2' : 'pacman-r 2.gif',
    'PACMANR_3' : 'pacman-r 3.gif',
    'PACMANR_4' : 'pacman-r 4.gif',
    'PACMANR_5' : 'pacman-r 5.gif',
    'PACMANR_6' : 'pacman-r 6.gif',
    'PACMANR_7' : 'pacman-r 7.gif',
    'PACMANR_8' : 'pacman-r 8.gif',
    'PACMANU_1' : 'pacman-u 1.gif',
    'PACMANU_2' : 'pacman-u 2.gif',
    'PACMANU_3' : 'pacman-u 3.gif',
    'PACMANU_4' : 'pacman-u 4.gif',
    'PACMANU_5' : 'pacman-u 5.gif',
    'PACMANU_6' : 'pacman-u 6.gif',
    'PACMANU_7' : 'pacman-u 7.gif',
    'PACMANU_8' : 'pacman-u 8.gif',
    'PACMAN' : 'pacman.gif',
    # Tiles
    'BLANK' : 'blank.gif',
    'DOOR_H' : 'door-h.gif',
    'DOOR_V' : 'door-v.gif',
    'PELLET' : 'pellet.gif',
    'PELLET_POWER' : 'pellet-power.gif',
    'SHOWLOGO' : 'showlogo.gif',
    'START' : 'start.gif',
    'WALL_CORNER_LL' : 'wall-corner-ll.gif',
    'WALL_CORNER_LR' : 'wall-corner-lr.gif',
    'WALL_CORNER_UL' : 'wall-corner-ul.gif',
    'WALL_CORNER_UR' : 'wall-corner-ur.gif',
    'WALL_END_B' : 'wall-end-b.gif',
    'WALL_END_L' : 'wall-end-l.gif',
    'WALL_END_R' : 'wall-end-r.gif',
    'WALL_END_T' : 'wall-end-t.gif',
    'WALL_NUB' : 'wall-nub.gif',
    'WALL_STRAIGHT_HORIZ' : 'wall-straight-horiz.gif',
    'WALL_STRAIGHT_VERT' : 'wall-straight-vert.gif',
    'WALL_T_BOTTOM' : 'wall-t-bottom.gif',
    'WALL_T_LEFT' : 'wall-t-left.gif',
    'WALL_T_RIGHT' : 'wall-t-right.gif',
    'WALL_T_TOP' : 'wall-t-top.gif',
    'WALL_X' : 'wall-x.gif',
    'X_PAINTWALL' : 'x-paintwall.gif', 
    # Backgrounds
    'BACKGROUND_BASE': 'background.gif',
    # Messages
    'DIGIT_0' : '0.gif',
    'DIGIT_1' : '1.gif',
    'DIGIT_2' : '2.gif',
    'DIGIT_3' : '3.gif',
    'DIGIT_4' : '4.gif',
    'DIGIT_5' : '5.gif',
    'DIGIT_6' : '6.gif',
    'DIGIT_7' : '7.gif',
    'DIGIT_8' : '8.gif',
    'DIGIT_9' : '9.gif',
    'GAMEOVER' : 'gameover.gif',
    'LIFE' : 'life.gif',
    'LOGO' : 'logo.gif',
    'READY' : 'ready.gif'
    }
    
SoundResourceDict = {
    'EATFRUIT' : 'eatfruit.wav',
    'EATGH2' : 'eatgh2.wav',
    'EXTRALIFE' : 'extralife.wav',
    'FRUITBOUNCE' : 'fruitbounce.wav',
    'PELLET_1' : 'pellet1.wav',
    'PELLET_2' : 'pellet2.wav',
    'POWERPELLET' : 'powerpellet.wav',
    'PARADOX' : 'paradox.wav'
    }

DefaultResourceDict = {
    'DEFAULT_LEVEL': 'default_level.json',                        
    'DEFAULT_IMAGE': 'default_image.gif', 
    'DEFAULT_SOUND': 'default_sound.wav' , 
    'DEFAULT_MESSAGE': 'default_message.gif' 
    }

################################################################
#  Definition of the resource class.
################################################################

class Resource(object):
    '''
        A resource contains: 
            a type to identify it within the program
            a file name and an optional id to load it from disk.
    '''
    def __init__(self,  type,  file_name,  file_key = None):
        '''
        Constructor
        '''
        self.type = type
        self.file_name = file_name
        self.file_key = file_key
        
    def is_valid(self):
        '''
        Checks 
            1. The type is valid
            2. The file name points to a valid file
        Returns false and asserts if one condition is not valid.
        '''
        import os
        return self.type in ResourceTypeDict and os.path.exists(self.file_name)
