#! /usr/bin/env python
################################################################
#  Tests Sound engine
################################################################

import unittest, resources
from enginesound import SoundEngine


class SomeSoundEngine(unittest.TestCase):
    def setUp(self):
        self.test_pelletnr = 0
        self.sound_resources = {}  
        folder_name = "./res/sounds/"
        for key in resources.SoundResourceKeyDict:
            my_resource = resources.Resource("SOUND", folder_name  + resources.SoundResourceKeyDict[key])
            if my_resource.is_valid():
                self.sound_resources[key] = my_resource
            else:
                self.sound_resources[key] = default_sound_resource()
        self.se = SoundEngine()
        self.se.load_resources(self.sound_resources)


    def test_play_sound_pellet(self):
        for x in range(0, 20):
            self.assertEqual(self.se.pelletnr, self.test_pelletnr)
            self.se.play_sound('PELLET')
            self.test_pelletnr = (self.test_pelletnr+1)%2

    def test_play_frame(self):
        self.se.play_frame(self.se.get_frame_sounds())
        self.assertEqual(self.se.get_frame_sounds(), list(self.sound_resources.keys()))
        

        
if __name__ == '__main__':
    unittest.main(verbosity = 2, buffer = True)
    # One could also use any of 
    # python -m unittest -v tests_vehicle
    # python -m unittest -v tests_vehicle.FullVehicle
    # python -m unittest -v tests_vehicle.FullVehicle.test_drive_short
