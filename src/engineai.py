################################################################
#  Artificial intelligence engine: implements different pursuit algorithms for the ghosts.
################################################################

from constants import DirectionKeys
from random import choice
from level import BoardSquare

class AIEngine(object):
    """
    Base AI engine, representing the personality of a ghost.
    The Wiki page on the pacman game described the different personalities.
    """
    def __init__(self):
        """
        """
        pass
        
    def calc_direction(self, my_position, other_ghosts_position, pacman_position, labyrinth_matrix):
        """  calc_direction(self, tuple(int), list(tuple(int)), tuple(int), list(list(BroadSquare))) -> DirectionKeys

        Calculate the next direction that a ghost should have, inside the
        labyrinth.
            my_position -- couple of integers of the ghost position.
            other_ghosts_position -- list of positions of the other ghosts.
            pacman_position -- couple of integers of the pacman position
            labyrinth_matrix -- matrix of BoardSquare-s (see level.py).
                BroadSquare.type: WALL = 1, SPACE = 0
        Returns one of the values in DirectionKeys.
            DIR_LEFT = 0
            DIR_RIGHT = 1
            DIR_UP = 2
            DIR_DOWN = 3
        """
        pass

    @staticmethod
    def get_random__possible_direction(my_position, labyrinth_matrix):
        """  get_random__possible_direction(tuple(int), list(list(BroadSquare))) -> DirectionKey

        Returns a random DirectionKey where there is a space
        """
        for _ in range(0, 20):
            next_direction = choice(DirectionKeys.ALL)
            if AIEngine.labyrinth_has_space_in_direction(my_position, labyrinth_matrix, next_direction):
                return next_direction
        return DirectionKeys.DIR_NONE #ERROR Ghost is stuck in between walls and has no space to go...

    @staticmethod
    def labyrinth_has_space_in_direction(my_position, labyrinth_matrix, direction_key):
        """  labyrinth_has_space_in_direction(tuple(int), list(list(BroadSquare)), DirectionKey) -> Boolean

        Returns true if the given Direction is free (space)
        """
        if (direction_key == DirectionKeys.DIR_LEFT
            and labyrinth_matrix[my_position[0]-1][my_position[1]].type == BoardSquare.SPACE):
            return True
        if (direction_key == DirectionKeys.DIR_RIGHT
            and labyrinth_matrix[my_position[0]+1][my_position[1]].type == BoardSquare.SPACE):
            return True
        if (direction_key == DirectionKeys.DIR_UP
            and labyrinth_matrix[my_position[0]][my_position[1]-1].type == BoardSquare.SPACE):
            return True
        if (direction_key == DirectionKeys.DIR_DOWN
            and labyrinth_matrix[my_position[0]][my_position[1]+1].type == BoardSquare.SPACE):
            return True
        return False

class AIChaser(AIEngine):
    """ AI class for the ghost named: Blinky (red guy)

    Strategy: Random pattern attack
    Blinky is nicknamed Pokey because after he leaves the box, he will move around the maze in random directions,
    but he will attack if Pac-Man is detected to be nearby. Sometimes, Inky will only attack when Blinky is attacking.
    """

    def __init__(self):
        """
        """
        pass
        
    def calc_direction(self, my_position, other_ghosts_position, pacman_position, labyrinth_matrix):
        """ calc_direction(self, tuple(int), list(tuple(int)), tuple(int), list(list(BroadSquare))) -> DirectionKeys

        Chasing calc_direction function. Chases after the pacman.
        """
        def has_space(direction_key): # Nested function defined to shorten calls to static method "labyrinth_has_space_in_direction"
            return AIEngine.labyrinth_has_space_in_direction(my_position, labyrinth_matrix, direction_key)

        
        pacman_position_relative_to_mine = [b - a for a, b in zip(my_position, pacman_position)]

        if pacman_position_relative_to_mine[0] < 0: # pacman is to my left
            if has_space(DirectionKeys.DIR_LEFT):
                return DirectionKeys.DIR_LEFT
        if pacman_position_relative_to_mine[0] > 0: # pacman is to my right
            if has_space(DirectionKeys.DIR_RIGHT):
                return DirectionKeys.DIR_RIGHT
        if pacman_position_relative_to_mine[1] > 0: # pacman is underneath me
            if has_space(DirectionKeys.DIR_DOWN):
                return DirectionKeys.DIR_DOWN              
        if pacman_position_relative_to_mine[1] < 0: # pacman is above me
            if has_space(DirectionKeys.DIR_UP):
                return DirectionKeys.DIR_UP
        
        return AIEngine.get_random__possible_direction(my_position, labyrinth_matrix)# If for some reason everything fails
    

class AIAmbusher(AIEngine):
    """ AI class for the ghost named: Pinky (pink guy)

    Strategy: Ambush
    Pinky is nicknamed Speedy and she will often follow Pac-Man's direction, not Pac-Man himself, then,
    she'll try her best to go around the walls and take you out.
    But, sometimes, she'll turn away if Pac-Man and her come face-to-face,
    in "scatter" mode, she often hangs in the top left corner.
    """

    def __init__(self):
        """
        """
        self.current_pacman_position = (0,0)
        self.last_pacman_position = (0,0)
        self.pacman_direction = DirectionKeys.DIR_UP
        
    def calc_direction(self, my_position, other_ghosts_position, pacman_position, labyrinth_matrix):
        """ calc_direction(self, tuple(int), list(tuple(int)), tuple(int), list(list(BroadSquare))) -> DirectionKeys

        Chasing calc_direction function. Ambushing strategy
        """
        def has_space(direction_key): # Nested function defined to shorten calls to static method "labyrinth_has_space_in_direction"
            return AIEngine.labyrinth_has_space_in_direction(my_position, labyrinth_matrix, direction_key)

        self.last_pacman_position = self.current_pacman_position
        self.current_pacman_position = pacman_position

        pacman_move_direction = [b - a for a, b in zip(self.last_pacman_position, self.current_pacman_position)]      
        if (pacman_move_direction[0] < 0 # pacman moves to the left
            and has_space(DirectionKeys.DIR_LEFT)):
                return DirectionKeys.DIR_LEFT
        if (pacman_move_direction[0] > 0 # pacman moves to the right
            and has_space(DirectionKeys.DIR_RIGHT)):
                return DirectionKeys.DIR_RIGHT
        if (pacman_move_direction[1] > 0 # pacman moves down
            and has_space(DirectionKeys.DIR_DOWN)):
                return DirectionKeys.DIR_DOWN              
        if (pacman_move_direction[1] < 0 # pacman moves up
            and has_space(DirectionKeys.DIR_UP)):
                return DirectionKeys.DIR_UP
        return AIEngine.get_random__possible_direction(my_position, labyrinth_matrix)    
    
    
class AIFickle(AIEngine):
    """ AI class for the ghost named: Inky (blue guy)

    Strategy: All
    If you read all of the other ghost strategies, you'll see that what makes Inky so dangerous
    is Blinky only attacks when Pac-Man is nearby, Pinky ambushes and Clyde wanders, but Inky does all of those,
    so he's extremely dangerous. He only will attack Pac-Man if Blinky is nearby or if Blinky is chasing Pac-Man.
    """
    PROXIMITY_DISTANCE = 5 #Setting Poximity Distance when to attack

    def __init__(self):
        """
        """
        pass
        
    def calc_direction(self, my_position, other_ghosts_position, pacman_position, labyrinth_matrix):
        """ calc_direction(self, tuple(int), list(tuple(int)), tuple(int), list(list(BroadSquare))) -> DirectionKeys

        Chasing calc_direction function. Different strategies
        """       
        def has_space(direction_key): # Nested function defined to shorten calls to static method "labyrinth_has_space_in_direction"
            return AIEngine.labyrinth_has_space_in_direction(my_position, labyrinth_matrix, direction_key)

        
        pacman_position_relative_to_mine = [b - a for a, b in zip(my_position, pacman_position)]
        
        if (abs(pacman_position_relative_to_mine[0]) < PROXIMITY_DISTANCE or
            abs(pacman_position_relative_to_mine[1]) < PROXIMITY_DISTANCE): #If pacman is close, start Chase strategy
            if pacman_position_relative_to_mine[0] < 0: # pacman is to my left
                if has_space(DirectionKeys.DIR_LEFT):
                    return DirectionKeys.DIR_LEFT
            if pacman_position_relative_to_mine[0] > 0: # pacman is to my right
                if has_space(DirectionKeys.DIR_RIGHT):
                    return DirectionKeys.DIR_RIGHT
            if pacman_position_relative_to_mine[1] > 0: # pacman is underneath me
                if has_space(DirectionKeys.DIR_DOWN):
                    return DirectionKeys.DIR_DOWN              
            if pacman_position_relative_to_mine[1] < 0: # pacman is above me
                if has_space(DirectionKeys.DIR_UP):
                    return DirectionKeys.DIR_UP
            return DirectionKeys.DIR_NONE
        else: # No chase mode
            return AIEngine.get_random__possible_direction(my_position, labyrinth_matrix)    

    
class AIStupid(AIEngine):
    """ AI class for the ghost named: Clyde (slow/red guy)
        
    Strategy: Sneak charge
    Clyde is nicknamed Shadow because he'll wander off alot, but he will attack and chase Pac-Man at any time,
    so be ready to expect him to come running at you.
    """

    def __init__(self):
        """
            
        """
        pass
        
    def calc_direction(self, my_position, other_ghosts_position, pacman_position, labyrinth_matrix):
        """ calc_direction(self, tuple(int), list(tuple(int)), tuple(int), list(list(BroadSquare))) -> DirectionKeys

        Random calc_direction function
        """
        return AIEngine.get_random__possible_direction(my_position, labyrinth_matrix)

    

class AIEscape(AIEngine):
    """ AI class for the moment when the ghosts try to avoid the pacman

    Strategy: Evasion
    """

    def __init__(self):
        """
        """
        pass

    
    def calc_direction(self, my_position, other_ghosts_position, pacman_position, labyrinth_matrix):
        """ calc_direction(self, tuple(int), list(tuple(int)), tuple(int), list(list(BroadSquare))) -> DirectionKeys

        Chasing calc_direction function. Avoids pacman
        """
        def has_space(direction_key): # Nested function defined to shorten calls to static method "labyrinth_has_space_in_direction"
            return AIEngine.labyrinth_has_space_in_direction(my_position, labyrinth_matrix, direction_key)

        
        pacman_position_relative_to_mine = [b - a for a, b in zip(my_position, pacman_position)]
        
        if pacman_position_relative_to_mine[0] < 0: # pacman is to my left
            if has_space(DirectionKeys.DIR_RIGHT):
                return DirectionKeys.DIR_RIGHT
        if pacman_position_relative_to_mine[0] > 0: # pacman is to my right
            if has_space(DirectionKeys.DIR_LEFT):
                return DirectionKeys.DIR_LEFT
        if pacman_position_relative_to_mine[1] > 0: # pacman is underneath me
            if has_space(DirectionKeys.DIR_UP):
                return DirectionKeys.DIR_UP               
        if pacman_position_relative_to_mine[1] < 0: # pacman is above me
            if has_space(DirectionKeys.DIR_DOWN):
                return DirectionKeys.DIR_DOWN
        
        return AIEngine.get_random__possible_direction(my_position, labyrinth_matrix)# If for some reason everything fails

