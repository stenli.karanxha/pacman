################################################################
#  Global imports
################################################################
import pygame

################################################################
#  Graphic engine
################################################################

class GraphicEngine(object):
    """
    """
    def __init__(self, screen_size):
        """ Constructor, with arguments: 
            screen_size -- size of the game area
        """
        self._window = pygame.display.set_mode(screen_size)
        pygame.display.set_caption("Pacman")
        self._screen = pygame.display.get_surface()
        self._surfaces = {}
        self._background_color = (0, 0, 0)

    def load_resources(self, image_resources):
        """ Load the image resources into a surface dictionary. """
        self._surfaces = {}
        for key in image_resources:
            self._surfaces[key] = pygame.image.load(image_resources[key].file_name).convert()
            
    def set_background_color(self, color):
        """ Defines the background color as a RGB sequence. """
        self._background_color = color
        
    def set_gif_substitution_colors(self, colors):
        pass

    def cleanup(self):
        pass

    def view_frame(self, image_descriptors):
        """ Send the surfaces, specified through a list of ImageDescriptor
        objects, to the screen.
        """
        self._screen.fill(self._background_color)
        for descriptor in image_descriptors:
            surface = self._surfaces[descriptor.key]
            self._screen.blit(surface, descriptor.position)
        pygame.display.flip()
    
