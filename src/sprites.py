################################################################
# Sprite family of classes: 2D animations, representing all the
# 2D objects visible in the game.
################################################################

from constants import DirectionKeys, ScreenDefinitions

class Velocities(object):
    """ Velocities of Pacman and the ghosts, as dictionaries with the 
    directions as keys."""
    
    BASE_PACMAN_VELOCITY = 55 / ScreenDefinitions.FRAMES_PER_SECOND
    BASE_GHOST_VELOCITY = 50.0 / ScreenDefinitions.FRAMES_PER_SECOND
    PACMAN_VELOCITIES = {
        DirectionKeys.DIR_NONE : [0.0, 0.0],
        DirectionKeys.DIR_LEFT : [-BASE_PACMAN_VELOCITY, 0.0],
        DirectionKeys.DIR_RIGHT : [BASE_PACMAN_VELOCITY, 0.0],
        DirectionKeys.DIR_UP : [0.0, -BASE_PACMAN_VELOCITY],
        DirectionKeys.DIR_DOWN : [0.0, BASE_PACMAN_VELOCITY]
    }
    GHOST_VELOCITIES = {
        DirectionKeys.DIR_NONE : [0.0, 0.0],
        DirectionKeys.DIR_LEFT : [-BASE_GHOST_VELOCITY, 0.0],
        DirectionKeys.DIR_RIGHT : [BASE_GHOST_VELOCITY, 0.0],
        DirectionKeys.DIR_UP : [0.0, -BASE_GHOST_VELOCITY],
        DirectionKeys.DIR_DOWN : [0.0, BASE_GHOST_VELOCITY]
    }


class ImageDescriptor(object):
    """ Data required to describe an image to the graphic engine."""
    
    def __init__(self, key, position):
        """
            Constructor with arguments:
            key -- unique identification of the image among all the rest of them.
            position -- position of the image in screen coordinates.
        """
        self.key = key
        self.position = [int(coord) for coord in position]
        
    def __str__(self):
        """ Transforms object into a string """
        rv = '{key: ' + str(self.key) + ' position: ' + str(self.position)
        return rv
        
    def shift_position(self, shift):
        """ Shift the position of the image. """
        self.position[0] += shift[0]
        self.position[1] += shift[1]


class Sprite(object):
    """ 2D animation, complete with control structures."""
    
    def __init__(self):
        """ Constructor without arguments.
        """
        self._position = [0.0, 0.0]
        self._frame_id = 0
        self._id = -1
        self._value = 0
    
    def animate(self):
        """ Return the image descriptor for the present frame, and 
        prepares the next animation frame. """
        if self._frame_id >= len(self._frames):
            self._frame_id = 0
        old_id = self._frame_id
        self._frame_id += 1
        return ImageDescriptor(self._frames[old_id], self._position)
        
    def get_id(self):
        """ Return the id of the sprite. """
        return self._id
    
    def get_value(self):
        """ Return the value in points of the sprite, meaningful for sprites 
        that can be eaten. """
        return self._value
    
    def is_food(self):
        return False


class FixedSprite(Sprite):
    """
    Base class for those sprites which do not have movement associated with them.
    """
    
    def __init__(self):
        super(FixedSprite, self).__init__()
    
    def set_direction(self, direction):
        pass
        
    def get_direction(self):
        DirectionKeys.DIR_NONE
        
    def get_position(self):
        return self._position
    
    def get_next_position(self):
        return self._position
    
    def move_to_position(self,  positon):
        pass


class MovableSprite(Sprite):
    
    def __init__(self, max_position):
        """ Constructor with arguments:
            max_position -- the maximum values of x and y before 
                wrapping happens.
        """
        self._max_position = max_position
        super(MovableSprite, self).__init__()
    
    def set_direction(self, direction):
        self._direction = direction
        
    def get_direction(self):
        return self._direction
    
    def get_position(self):
        return self._position
    
    def get_next_position(self):
        return [self._calc_coordinate(i) for i in range(2)]
    
    def _calc_coordinate(self, index):
        rv = self._position[index] + self._velocity[index]
        while rv < 0:
            rv += self._max_position[index]
        while rv > self._max_position[index]:
            rv -= self._max_position[index]
        return rv
    
    def move_to_position(self,  position):
        self._position = position


class Blank(FixedSprite):
   
    def __init__(self, position, frames):
        super(Blank, self).__init__()
        self._position = position
        self._frames = frames


class Wall(FixedSprite):
   
    def __init__(self, position, frames):
        super(Wall, self).__init__()
        self._position = position
        self._frames = frames
    

class Door(FixedSprite):
   
    def __init__(self, position, frames):
        super(Door, self).__init__()
        self._position = position
        self._frames = frames    
    

class Pellet(FixedSprite):
    
    def __init__(self, position, id, value, frames):
        super(Pellet, self).__init__()
        self._position = position
        self._id = id
        self._value = value
        self._frames = frames
        
    def is_food(self):
        return True
        

class Fruit(FixedSprite):
    
    def __init__(self, position, id, value, frames, life_definition):
        super(Fruit, self).__init__()
        self._position = position
        self._id = id
        self._value = value
        self._frames = frames
        self._life_start = life_definition[0]
        self._life_end = life_definition[1]
        self._life_count = 0

    def animate(self):
        if self._life_count < self._life_start:
            self._life_count += 1
            return None
        elif self._life_count < self._life_end:
            self._life_count += 1
            return super(Fruit, self).animate()
        else:
            return None
        
    def is_unborn(self):
        return self._life_count < self._life_start
        
    def is_alive(self):
        return self._life_count >= self._life_start and self._life_count < self._life_end
    
    def is_dead(self):
        return self._life_count >= self._life_end
    
    def is_food(self):
        return True


class GhostStates(object):
    DANGEROUS = 0
    SAFE = 1
    DEAD = 2


class Ghost(MovableSprite):
    
    def __init__(self, position, max_position, id, all_frames, direction = DirectionKeys.DIR_NONE):
        """
            Constructor with argumens:
        """
        super(Ghost, self).__init__(max_position)
        self._position = position
        self._id = id
        self._all_frames = all_frames
        self._frames = []
        self._state = GhostStates.DANGEROUS
        self._safe_state_counter = 0
        self._value = 0
        self.set_direction(direction)        
        self._select_frame_group()

    def animate(self):
        if self._safe_state_counter > 0:
            self._safe_state_counter -= 1
        elif not self.is_dangerous():
            self.set_state(GhostStates.DANGEROUS)
        return super(Ghost, self).animate()
        
    def set_direction(self, direction):
        """ Change the direction and velocity of the ghost, 
            only if it is not in the DEAD state.
        """
        if self._state != GhostStates.DEAD:
            assert(direction in DirectionKeys.ALL)
            self._direction = direction
            self._velocity = Velocities.GHOST_VELOCITIES[direction]
        
    def set_state(self, state):
        self._state = state
        self._select_frame_group()
        
    def get_state(self):
        return self._state

    def set_safe_state_length(self, safe_state_length):
        self._safe_state_counter = safe_state_length
        
    def set_food_value(self, food_value):
        self._value = food_value

    def is_dangerous(self):
        return self._state == GhostStates.DANGEROUS
    
    def is_safe(self):
        return self._state != GhostStates.DANGEROUS
      
    def _select_frame_group(self):
        """ From all the frames, select those relevant to the state."""
        self._frames = self._all_frames[self._state]


class Pacman(MovableSprite):

    def __init__(self, position, max_position, id, all_frames, direction = DirectionKeys.DIR_NONE):
        """
            Constructor with argumens:
        """
        super(Pacman, self).__init__(max_position)
        self._position = position
        self._id = id
        self._all_frames = all_frames
        self._frames = []
        self._direction = direction
        self._velocity = Velocities.PACMAN_VELOCITIES[direction]
        self._select_frame_group()

    def set_direction(self, direction):
        """
            Change the direction of pacman, and the block of 
            frames assigned to the particular direction.
        """
        assert(direction in DirectionKeys.ALL)
        if direction != self._direction:
            self._direction = direction
            self._velocity = Velocities.PACMAN_VELOCITIES[direction]
            self._select_frame_group()
      
    def _select_frame_group(self):
        """ From all the frames, select those relevant to the direction. """
        assert(self._direction in DirectionKeys.ALL)
        self._frames = self._all_frames[self._direction]
