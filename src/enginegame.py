################################################################
#  Game engine
################################################################
from level import LevelInit, Level, FruitLife, LevStatKeys
import engineai
from constants import DirectionKeys, ScreenDefinitions, GhostKeys, PacmanKeys
from sprites import ImageDescriptor

# Points required to get a new life for Pacman
POINTS_FOR_NEXT_LIFE = 10000

# Fruit life and periodicity, in game ticks.
FRUIT_LIFE = 5 * ScreenDefinitions.FRAMES_PER_SECOND  # 5 seconds
FRUIT_PERIODICITY = 60 * ScreenDefinitions.FRAMES_PER_SECOND  # 60 seconds

# Power-up duration and reduction, in game ticks.
BASE_POWER_UP_TIME = 10.0 * ScreenDefinitions.FRAMES_PER_SECOND   # 10 seconds
POWER_UP_REDUCTION = 0.5 * ScreenDefinitions.FRAMES_PER_SECOND  # 0.5 seconds

class GameEngine(object):
    """
    Contains the functionality relevant to the whole game, seen as 
    a set of levels. 
    """
    
    def __init__(self):
        """
        Constructor with no arguments.
        """
        # Level initialization data
        self._levels_init = None
        # AI engines
        self._ai_chaser = engineai.AIChaser()
        self._ai_ambusher = engineai.AIAmbusher()
        self._ai_fickle = engineai.AIFickle()
        self._ai_stupid = engineai.AIStupid()
        self.ai_escape = engineai.AIEscape()

    def load_resources(self,  resources):
        self._levels_init = []
        for i in range(255):
            key = 'LEVEL_' + str(i)
            if key in resources:
                my_level_init = LevelInit()
                my_level_init.load_from_file(resources[key].file_name)
                if my_level_init.is_valid():
                    self._levels_init.append(my_level_init)
        
    def start(self):
        """
            Initialize the game: set level to 0, initialize the level with the 
            start-up info, and initialize the game statistics. 
        """
        # Sanity check of the level init:
        if self._levels_init == None or len(self._levels_init) == 0:
            assert(False)  # Wrong sequence of calls or initializations
            return False
        
        # Level info
        self._current_level_id = 0
        self._current_level = None
        self._init_current_level()
        # Game statistics
        self._total_points = 0
        self._prev_levels_points = 0
        self._points_for_next_life = POINTS_FOR_NEXT_LIFE
        self._available_lives = 3
        self._available_fruits = 2
        # Current images of the sprites and game statistics
        self._current_image_descriptors = []
        # Sound events
        self._sound_event = ''
        return True

    def cleanup(self):
        pass

    def generate_frame(self,  keyboard_inputs):
        """
            Power horse function for the game engine: generates a frame of the game.
        """
        # Sets the pacman direction based on the keyboard.
        self._direction = keyboard_inputs.get_last_dir_key_pressed()
        self._current_level.set_pacman_direction(self._direction)
        
        # Calculates the ghosts direction, using the ai algorithm assigned to each.
        assigned_ai = self._assign_ai_to_ghost()
        for ghost_id in assigned_ai:
            direction = self._calc_ghost_direction(ghost_id, assigned_ai[ghost_id])
            self._current_level.set_ghost_direction(ghost_id, direction)
        
        # Gets the future position of all the movable sprites.
        next_positions = self._current_level.get_next_position_for_all()
        
        # Detects the collisions of Pacman
        pacman_next_position = next_positions[PacmanKeys.PAC0]
        colliding_wall = self._current_level.cd_pacman_with_wall(pacman_next_position)
        colliding_ghost = self._current_level.cd_pacman_with_ghost(pacman_next_position)
        colliding_pellet = self._current_level.cd_pacman_with_pellet(pacman_next_position)
        colliding_fruit = self._current_level.cd_pacman_with_fruit(pacman_next_position)
        
        # Refreshes the fruits
        self._current_level.refresh_fruits()
        
        # Refreshes the level statistics and status from the collisions
        if colliding_wall:
            del next_positions[PacmanKeys.PAC0]
        elif colliding_ghost:
            if colliding_ghost.is_dangerous():
                self._available_lives -= 1
                self._current_level.reinitialize_pacman()
                del next_positions[PacmanKeys.PAC0]
            else:
                self._current_level.eat_ghost(colliding_ghost.get_id())
        elif colliding_pellet:
            self._current_level.eat_food(colliding_pellet)
        elif colliding_fruit:
            self._current_level.eat_fruit(colliding_fruit)
        
        # Refreshes the game statistics and state from the level statistics
        level_points = self._current_level.get_statistic(LevStatKeys.POINTS_GAINED)
        self._total_points = self._prev_levels_points + level_points
        
        self._available_fruits = self._current_level.get_statistic(LevStatKeys.FRUITS_LEFT)

        if self._total_points >= self._points_for_next_life:
            self._available_lives += 1
            self._points_for_next_life += POINTS_FOR_NEXT_LIFE
        
        # Move all to the next position
        self._current_level.move_all_to_position(next_positions)
        
        # Reads the sprite images
        self._current_image_descriptors = self._current_level.animate()
        self._shift_board_images(self._current_image_descriptors)
        
        # Adds the game stats images
        self._draw_lives()
        self._draw_fruits()
        self._draw_total_points()
        self._draw_level()
        
        # Checks and eventually changes the level.
        if self._current_level.get_statistic(LevStatKeys.PELLETS_LEFT) == 0:
            self._prev_levels_points = self._total_points
            self._current_level_id += 1
            self._init_current_level()

        # Decision if the game should stop        
        return self._available_lives == 0
        
    def get_frame_images(self):
        return self._current_image_descriptors
        
    def get_frame_sounds(self):
        return self._sound_event
        
    def _init_current_level(self):
        id = self._current_level_id % len(self._levels_init)
        fruit_lives = self._get_fruit_lives()
        powerup_time = self._get_power_up_time()
        if self._current_level:
            del self._current_level
        self._current_level = Level(self._levels_init[id], fruit_lives, powerup_time)
        
    # Fruit management
    def _get_fruit_lives(self):
        """ Generate and return the fruit lives, depending on the number of 
        fruits that should be available for this level. """
        num_fruits = min([self._current_level_id + 2, 8])
        lives = [FruitLife(FRUIT_PERIODICITY * i, FRUIT_PERIODICITY * i + FRUIT_LIFE) for i in range(num_fruits)]
        return lives
        
    # Ghost management
    def _assign_ai_to_ghost(self):
        """
            Assign an ai engine (personality) to each ghost. At present the 
            assignment is fixed, based on either the ghosts are dangerous or
            safe to eat, but it is easy to add a random change based on the level.
        """
        if self._current_level.is_powerup_in_effect():
            return {
                  GhostKeys.BLINKY:  self.ai_escape,
                  GhostKeys.PINKY:  self.ai_escape,
                  GhostKeys.INKY:  self.ai_escape,
                  GhostKeys.CLYDE:  self.ai_escape
                  }            
        else:
            return {
                  GhostKeys.BLINKY:  self._ai_chaser,
                  GhostKeys.PINKY:  self._ai_ambusher,
                  GhostKeys.INKY:  self._ai_fickle,
                  GhostKeys.CLYDE:  self._ai_stupid
                  }

    def _calc_ghost_direction(self,  ghost_id,  ai_engine):
        """ Return the future direction for a given ghost, depending on his personality, 
        and on the time passed since the start of the level. """
        all_positions = self._current_level.get_ghosts_position()
        assert(ghost_id in all_positions)
        my_position = list(all_positions[ghost_id])
        del all_positions[ghost_id]
        pacman_position = self._current_level.get_pacman_position()
        labyrinth_matrix = self._current_level.get_labyrinth()
        #return ai_engine.calc_direction(my_position, all_positions, pacman_position, labyrinth_matrix)
        return DirectionKeys.DIR_NONE

    def _get_power_up_time(self):
        """ Return the length of the power-up time, depending on the level. """
        time = BASE_POWER_UP_TIME - self._current_level_id * POWER_UP_REDUCTION
        return max([time, 0])
    
    def _shift_board_images(self, image_descriptors):
        """ Apply a shift to transform the board coordinates of the images 
            into the screen coordinates.
        """
        self.shift(ScreenDefinitions.BOARD_BASE_POSITION, image_descriptors)

    def _draw_lives(self):
        """ Generate and add the images for the still available lives. """
        image_descriptors = []
        for i in range(self._available_lives):
            position = ScreenDefinitions.get_increased_position(i)
            image_descriptor = ImageDescriptor('LIFE', position)
            image_descriptors.append(image_descriptor)
        self.shift(ScreenDefinitions.LIVES_BASE_POSITION, image_descriptors)
        self._current_image_descriptors.extend(image_descriptors)
        
    def _draw_fruits(self):
        """ Generate and add the images for the still available fruits. """
        image_descriptors = []
        fruit_id = self._current_level.get_fruit_id()
        for i in range(self._available_fruits):
            position = ScreenDefinitions.get_increased_position(i)
            image_descriptor = ImageDescriptor(fruit_id, position)
            image_descriptors.append(image_descriptor)
        self.shift(ScreenDefinitions.FRUITS_BASE_POSITION, image_descriptors)
        self._current_image_descriptors.extend(image_descriptors)
        
    def _draw_total_points(self):
        """Generate and add the images for the points done in the whole game. """
        image_descriptors = self.number_to_image_descriptors(self._total_points)
        self.shift(ScreenDefinitions.TOTAL_POINTS_BASE_POSITION, image_descriptors)
        self._current_image_descriptors.extend(image_descriptors)
        
    def _draw_level(self):
        """Generate and add the images for the level number. """
        image_descriptors = self.number_to_image_descriptors(self._current_level_id)
        self.shift(ScreenDefinitions.LEVEL_BASE_POSITION, image_descriptors)
        self._current_image_descriptors.extend(image_descriptors)

    @staticmethod
    def number_to_image_descriptors(number):
        """ Transform an integer to a list of images. """
        my_string = str(number)
        image_descriptors = []
        for index, digit in enumerate(my_string):
            position = [coord * index for coord in ScreenDefinitions.INCREASE_POSITION]
            image_descriptor = ImageDescriptor('DIGIT_' + str(int(digit)), position)
            image_descriptors.append(image_descriptor)
        return image_descriptors

    @staticmethod
    def shift(shift, image_descriptors):
        """ Shift all the image positions by the shift coefficient. """
        for descriptor in image_descriptors:
            descriptor.shift_position(shift)
